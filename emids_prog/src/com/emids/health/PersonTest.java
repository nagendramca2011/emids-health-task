package com.emids.health;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.junit.Test;

public class PersonTest {

	InsuranceExpectedResult expectedResult=new InsuranceExpectedResult();
	Person person = new Person();
	
	public PersonTest() {
		
		String gender = "";
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		try {
			System.out.println("Enter Customer Name :");
			person.setName(br.readLine());
			System.out.println("Enter Gender (Male,Female,Other):");
			gender = br.readLine();
			System.out.println("Enter Age:");
			person.setAge(Integer.parseInt(br.readLine()));
			System.out.println("**Your Current Health**");
			System.out.println("HyperTension(Yes/No):");
			person.setHyperTension("Yes".equalsIgnoreCase(br.readLine()) ? true
					: false);
			System.out.println("BloodPressure(Yes/No):");
			person.setBloodPressure("Yes".equalsIgnoreCase(br.readLine()) ? true
					: false);
			System.out.println("BloodSugar(Yes/No):");
			person.setBloodSugar("Yes".equalsIgnoreCase(br.readLine()) ? true
					: false);
			System.out.println("OverWeight(Yes/No):");
			person.setOverWeight("Yes".equalsIgnoreCase(br.readLine()) ? true
					: false);

			System.out.println("**Your Habits**");
			System.out.println("Smoke(Yes/No):");
			person.setSmoking("Yes".equalsIgnoreCase(br.readLine()) ? true
					: false);
			System.out.println("Alcohol(Yes/No):");
			person.setAlcohol("Yes".equalsIgnoreCase(br.readLine()) ? true
					: false);
			System.out.println("Daily Exercise(Yes/No):");
			person.setAlcohol("Yes".equalsIgnoreCase(br.readLine()) ? true
					: false);
			System.out.println("Drugs(Yes/No):");
			person.setDrugs("Yes".equalsIgnoreCase(br.readLine()) ? true
					: false);

			if (gender.equalsIgnoreCase("Male")) {
				person.setGender(Sex.MALE);
			} else if (gender.equalsIgnoreCase("Female")) {
				person.setGender(Sex.FEMALE);
			} else {
				person.setGender(Sex.OTHER);
			}

			/*PersonTest personTest=new PersonTest();
			personTest.testGetName();*/
			
			System.out.println("Health Insurance Premium for Mr."  + person.getName() + ": Rs." + InsuranceCalulation.calculateInsurance(person));
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
			

	}
	
	@Test
	public void testGetName() {
		assertEquals(expectedResult.getName(), person.getName());
	}

	
}
