package com.emids.health;



import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

public class InsuranceCalulation {
	public static final int BASE_PREMIUM = 5000;

	public static double calculateInsurance(Person p) {
		
		CheckPersonEligibilityForSelectiveService person = new CheckPersonEligibilityForSelectiveService();		
		 
		return BASE_PREMIUM
				+ person.getPersonAgePremiumAmount(p, BASE_PREMIUM)
				+ person.getPersonGenderPremiumAmount(p, BASE_PREMIUM)
				+ person.getPersonHabitsPremiumAmount(p, BASE_PREMIUM)
				+ person.getPersonHealthPremiumAmount(p, BASE_PREMIUM);

	}

	public static void main(String[] args) {			//Test Case
			
			Result result = JUnitCore.runClasses(PersonTest.class);
			
		      for (Failure failure : result.getFailures()) {
		         System.out.println(failure.toString());
		      }
				
		      System.out.println(" print results "+result.wasSuccessful());
	}
		
		      
}
